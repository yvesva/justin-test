from pathlib import PurePath
import os

parts = PurePath(__file__).parts

proj_root = os.path.join(*parts[:-2])

print(proj_root)

data = os.path.join(proj_root, 'data', 'stuff.csv')

print(data)
